/* eslint-disable no-console */
const fs = require('fs');
const util = require('util');
const express = require('express');
const bodyparser = require('body-parser');


let changes = [];
let logs = [];

if (fs.existsSync('changes.json'))
    changes = JSON.parse(fs.readFileSync('changes.json'));

if (fs.existsSync('logs.json'))
    logs = JSON.parse(fs.readFileSync('logs.json'));

changes.add = function(html) {
    this.push(html);
    fs.writeFileSync('changes.json', JSON.stringify(this));
    logs.add(html);
    
}.bind(changes);

logs.add = function(html) {
    this.push(html);
    fs.writeFileSync('logs.json', JSON.stringify(this));
    
}.bind(logs);


const app = express();

app.use(express.json());

app.post('/api/fetchchanges', (req, res) => {
    res.contentType = 'application/json';

    let newChanges = changes.length - req.body.pos;

    if (newChanges > 0) {
        console.log(`- ${req.headers['x-forwarded-for'] || req.connection.remoteAddress} got ${newChanges} new deltas.`);
        res.send(JSON.stringify({ data: changes.slice(req.body.pos), status: 0 }));
    }
    
    else if (changes.length === req.body.pos) {
        res.send(JSON.stringify({ data: [], status: 0 }));
    }
    
    else {
        res.send(JSON.stringify({ data: changes, status: 1 }));
        console.log(`- ${req.headers['x-forwarded-for'] || req.connection.remoteAddress} got ${changes.length} deltas.`);
    }
});

app.post('/api/addchange', (req) => {
    if (req.body.data.length < 1024 * 5) {
        changes.add(req.body);

        console.log(`- ${req.headers['x-forwarded-for'] || req.connection.remoteAddress}${req.body.author != null ? ` (\'${req.body.author}\')` : ''} added: ${util.inspect(req.body.data)}`);
    }
});

app.get('/', (req, res) => {
    res.send(fs.readFileSync('index.html', 'utf-8'));   
});

app.listen(5020, () => console.log('Listening on port 5020.'));

setInterval(() => { while (changes.length > 0) changes.pop(); fs.writeFileSync('changes.json', JSON.stringify(changes)); console.log('Reset data.'); }, 10 * 60 * 1000);